/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import {Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid";
const window= Dimensions.get('window');


var valueArray=[]
export default class Grids extends Component {

constructor(){
  super()
  this.state = {
    number:0
  }
}
  getValue(val) {
    var tempValue = this.state.number;
    if (tempValue == 0) {
      this.setState({number:tempValue+val})
    }
    else {
      this.setState({number:tempValue+val.toString()})
    }
  ///valueArray.push(val);
    console.log('ssss ',val);
  }

  addFunction() {
    var tempValue = this.state.number;
    if (tempValue == 0) {
      this.setState({number:tempValue+'+'})
    }
    else {
      this.setState({number:tempValue+'+'})
    }
  }

  calculate(){
    var tempValue = this.state.number.split('+');
    var data = parseInt(tempValue[0])+ parseInt(tempValue[1])
    this.setState({data:data})
    console.log('data ',data);
  }

  render() {
    const PadNumber={
      textAlign:'center',
      color:'#ff9933',
      marginTop:30,
      fontSize:15
    }
    const PadColumn={
      height:75
    }
    const ResultField={
      backgroundColor:'black',
      color:'#fff',
      height:80,
      width:window.width


    }
    const ResultField2={
      backgroundColor:'black',
      color:'#fff',
      height:20,
      width:window.width


    }

  return (
      <Container>
        <Header>
          <Left>
            <Button transparent>

            </Button>
          </Left>
          <Body>
            <Title>Header</Title>
          </Body>
          <Right />
        </Header>
        <Content>
        <Grid>
        <Row>

        <Col style={PadColumn}>
            <Text style={ResultField}>{this.state.number}</Text>


        </Col>
        </Row>

      <Row>

      <Col style={PadColumn}>
          <TouchableOpacity onPress={()=> this.getValue(1)}><Text style={PadNumber}>1</Text></TouchableOpacity>
      </Col>
    <Col style={PadColumn}>
          <TouchableOpacity onPress={()=> this.getValue(2)}><Text style={PadNumber}>2</Text></TouchableOpacity>
      </Col>
    <Col style={PadColumn}>
          <TouchableOpacity onPress={()=> this.getValue(3)}><Text style={PadNumber}>3</Text></TouchableOpacity>
      </Col>
      </Row>
      <Row>
    <Col style={PadColumn}>
          <TouchableOpacity onPress={()=> this.getValue(4)}><Text style={PadNumber}>4</Text></TouchableOpacity>
      </Col>
    <Col style={PadColumn}>
          <TouchableOpacity onPress={()=> this.getValue(5)}><Text style={PadNumber}>5</Text></TouchableOpacity>
      </Col>
    <Col style={PadColumn}>
          <TouchableOpacity onPress={()=> this.getValue(6)}><Text style={PadNumber}>6</Text></TouchableOpacity>
      </Col>
      </Row>
      <Row>
    <Col style={PadColumn}>
          <TouchableOpacity onPress={()=> this.getValue(7)}><Text style={PadNumber}>7</Text></TouchableOpacity>
      </Col>
    <Col style={PadColumn}>
          <TouchableOpacity onPress={()=> this.getValue(8)}><Text style={PadNumber}>8</Text></TouchableOpacity>
      </Col>
    <Col style={PadColumn}>
          <TouchableOpacity  onPress={()=> this.getValue(9)}><Text style={PadNumber}>9</Text></TouchableOpacity>
      </Col>
      </Row>
      <Row>
    <Col style={PadColumn}>
          <TouchableOpacity onPress={()=> this.getValue(0)}>><Text style={PadNumber}>0</Text></TouchableOpacity>
      </Col>
    <Col style={PadColumn}>
          <TouchableOpacity onPress={()=> this.getValue(.)}>><Text style={[PadNumber,{marginTop:15,fontSize:30}]}>.</Text></TouchableOpacity>
      </Col>
    <Col style={PadColumn}>
          <TouchableOpacity><Text style={PadNumber}>Clear</Text></TouchableOpacity>
      </Col>


      </Row>
      <Row>
    <Col style={PadColumn}>
          <TouchableOpacity onPress={()=> this.addFunction()}><Icon name='ios-add' style={PadNumber}/></TouchableOpacity>
      </Col>
      <Col style={PadColumn}>
            <TouchableOpacity><Icon name='ios-remove' style={PadNumber}/></TouchableOpacity>
        </Col>
    <Col style={PadColumn}>
          <TouchableOpacity><Text style={PadNumber}>/</Text></TouchableOpacity>
      </Col>
    <Col style={PadColumn}>
          <TouchableOpacity><Icon name='md-close' style={PadNumber}/></TouchableOpacity>
      </Col>
      </Row>
      <Row>
      <Col style={PadColumn} >
            <TouchableOpacity onPress={()=> this.calculate()}><Text style={PadNumber}>Calculate</Text></TouchableOpacity>
            {this.state.data && <Text style={ResultField2}>{this.state.data}</Text>}
        </Col>
      </Row>
       </Grid>
        </Content>
        <Footer>
          <FooterTab>
            <Button full>

            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    textAlign:'center',
    color:'#ff9933',
    marginTop:30,
    fontSize:15
  }
});

AppRegistry.registerComponent('Grids', () => Grids);
