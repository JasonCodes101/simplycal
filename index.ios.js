/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

import Grids from './components/Grid/Index'

export default class SimplyCal extends Component {
  render() {
    return (
        <Grids/>
    );
  }
}

const styles = StyleSheet.create({

});

AppRegistry.registerComponent('SimplyCal', () => SimplyCal);
